[[_TOC_]]

# Terms
## Key
- sparse
- compound
- partial
- multikey

# Kinds

## SQL

### MySQL

### SQLite

### PostgreSQL

## Document

### MongoDB

### CouchDB

### Elasticsearch

## Key-value

### localStorage, sessionStorage

### Redis

### DynamoDB

### Riak

### Project Voldemort

## Wide-column

IoT, User Profile

### Casandra

### HBase

### CosmoDB

## Graph

### Neo4j

### JanusGraph

### OrientDB

### RedisGraph

## Resources

https://www.mongodb.com/nosql-explained

https://phoenixnap.com/kb/nosql-database-types
