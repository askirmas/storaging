import mongoose from "mongoose"

import User, {tUser} from "./user.model"

mongoose.set("debug", true)
mongoose.set("bufferCommands", false)

mongoose.connect("mongodb://localhost/test", {
  "useNewUrlParser": true,
  "useUnifiedTopology": true
})

const user1Data: tUser = {
  "email": "user1@example.com",
  "firstName": "v1",
  "lastName": "v2"
}

const db = mongoose.connection
db.on("error", console.error.bind(console, "connection error:"))
db.once("open", async () => {
    console.log("opened")
    const user1 = (await User.findOne(user1Data)) ?? new User(user1Data)
    
    const etcProps: string[] = []
    , ownProps: string[] = []
    for (const key in user1)
      (
        user1.hasOwnProperty(key)
        ? ownProps
        : etcProps
      ).push(key)
    
    // console.log({ownProps, etcProps})

    try {
      if (!user1.isModified())
        await User.deleteOne(user1Data)
      console.log(await User.count({}))
      await User.syncIndexes()
      console.log('deleted')
      await user1.save()
      console.log('saved')
      await db.close()  
    } catch(e) {
      console.error(e)
      process.exit(1)
    }
    console.log('ok')
})