import mongoose, { Schema, Document } from 'mongoose';

interface Address extends Document {
  street: string;
  city: string;
  postCode: string;
}

export type tUser = {
  email: string;
  firstName: string;
  lastName: string;
  address?: Address;
}

interface IUser extends Document, tUser {}

export default mongoose.model<IUser>('User', new Schema({
  "email": {
    "type": "String",
    "required": true,
    "unique": true
  },
  "firstName": {
    "type": "String",
    "required": true
  },
  "lastName": {
    "type": "String",
    "required": true
  },
  "address": {
    "street": {
      "type": "String"
    },
    "city": {
      "type": "String"
    },
    "postCode": {
      "type": "String"
    }
  }
}))
