# Typing
- https://medium.com/@tomanagle/strongly-typed-models-with-mongoose-and-typescript-7bc2f7197722
  - https://github.com/tomanagle/Mongoose-TypeScript-example.git
- https://github.com/typegoose/typegoose
- https://jestjs.io/docs/en/mongodb
  - https://www.npmjs.com/package/@shelf/jest-mongodb
- https://jestjs.io/docs/en/configuration#globalsetup-string
